/* This file is part of libinetiter.
 * Copyright (C) 2017 Julien JPK <julienjpk@email.com>

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or any later version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.

 * You should have received a copy of the GNU Affero General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "inetiter.h"

void _inet_addr_inc(inet_addr_t* addr){
    if(!addr) return;

    if(addr->ip.raw.sa_family == AF_INET){
	struct sockaddr_in* addr4 = &addr->ip.v4;
	addr4->sin_addr.s_addr = htonl(ntohl(addr4->sin_addr.s_addr) + 1);
    }
    else{
	struct sockaddr_in6* addr6 = &addr->ip.v6;
	unsigned char *addr6buf = addr6->sin6_addr.s6_addr;
	short i;
	
	for(i = 15; i >= 0 && addr6buf[i] == 0xff; i--) addr6buf[i] = 0;
	if(i == 0) i = 15;
	addr6buf[i] += 1;
    }
}

void _inet_addr_dec(inet_addr_t* addr){
    if(!addr) return;
    
    if(addr->ip.raw.sa_family == AF_INET){
	struct sockaddr_in* addr4 = &addr->ip.v4;
	addr4->sin_addr.s_addr = htonl(ntohl(addr4->sin_addr.s_addr) - 1);
    }
    else{
	struct sockaddr_in6* addr6 = &addr->ip.v6;
	unsigned char *addr6buf = addr6->sin6_addr.s6_addr;
	short i;
	
	for(i = 15; i >= 0 && addr6buf[i] == 0; i--) addr6buf[i] = 0xff;
	if(i == 0) i = 15;
	addr6buf[i] -= 1;
    }
}

int _inet_addr_cmp(inet_addr_t* addr1, inet_addr_t* addr2){
    if(!addr1 || !addr2) return addr1 == addr2;
    short diff;

    if(addr1->ip.raw.sa_family == AF_INET){
	diff = ntohl(addr1->ip.v4.sin_addr.s_addr);
	diff -= ntohl(addr2->ip.v4.sin_addr.s_addr);
    }
    else{
	unsigned char *addrbuf1 = addr1->ip.v6.sin6_addr.s6_addr;
	unsigned char *addrbuf2 = addr2->ip.v6.sin6_addr.s6_addr;
	short i;

	for(i = 15; i > 0 && addrbuf1[i] == addrbuf2[i]; i--);
	diff = addrbuf1[i] - addrbuf2[i];
    }

    return diff == 0 ? 0 : (diff > 0 ? 1 : -1);
}

socklen_t inet_addr_sizeof(inet_addr_t* addr){
    if(!addr) return 0;

    return addr->ip.raw.sa_family == AF_INET ?
	sizeof(addr->ip.v4) : sizeof(addr->ip.v6);
}

void inet_addr_set_port(inet_addr_t* addr, in_port_t port){
    if(!addr) return;
    port = htons(port);

    if(addr->ip.raw.sa_family == AF_INET) addr->ip.v4.sin_port = port;
    else addr->ip.v6.sin6_port = port;
}

char* inet_addr_str(inet_addr_t* addr, char* buffer){
    if(!addr) return NULL;
    
    char *addrstr;
    sa_family_t family = addr->ip.raw.sa_family;
    size_t len = family  == AF_INET ? INET_ADDRSTRLEN : INET6_ADDRSTRLEN;
    
    if(!buffer){
	addrstr = malloc(len);
	memset(addrstr, 0, len);
    }
    else addrstr = buffer;

    void* addrdata = family == AF_INET ? (void*)&(addr->ip.v4.sin_addr) :
	(void*)&(addr->ip.v6.sin6_addr);

    if(!inet_ntop(addr->ip.raw.sa_family, addrdata, addrstr, len)){
	if(!buffer) free(addrstr);
	return NULL;
    }

    return addrstr;
}

inet_network_t* inet_network_new(struct ifaddrs* itf){
    if(!itf) return NULL;

    sa_family_t family = itf->ifa_addr->sa_family;
    struct sockaddr* mask = itf->ifa_netmask;

    inet_network_t* net = malloc(sizeof(inet_network_t));
    memset(net, 0, sizeof(inet_network_t));

    if(family == AF_INET){
	memcpy(&net->first.ip.v4, itf->ifa_addr, sizeof(net->first.ip.v4));
	memcpy(&net->here.ip.v4, itf->ifa_addr, sizeof(net->here.ip.v4));

	struct sockaddr_in* first4 = &net->first.ip.v4;
	struct sockaddr_in* last4 = &net->last.ip.v4;
	struct sockaddr_in* mask4 = (struct sockaddr_in*)mask;
	first4->sin_addr.s_addr &= mask4->sin_addr.s_addr;

	if(!itf->ifa_broadaddr){
	    memcpy(&net->last.ip.v4, &net->first.ip.v4,
		   sizeof(net->last.ip.v4));
	    last4->sin_addr.s_addr |= ~(mask4->sin_addr.s_addr);
	}
	else memcpy(&net->last.ip.v4, itf->ifa_broadaddr,
		    sizeof(net->last.ip.v4));
    }
    else{
	memcpy(&net->first.ip.v6, itf->ifa_addr, sizeof(net->first.ip.v6));
	memcpy(&net->here.ip.v6, itf->ifa_addr, sizeof(net->here.ip.v6));
	
	struct sockaddr_in6* first6 = &net->first.ip.v6;
	struct sockaddr_in6* last6 = &net->last.ip.v6;
	struct sockaddr_in6* mask6 = (struct sockaddr_in6*)mask;
	unsigned char *firstbuf = first6->sin6_addr.s6_addr;
	unsigned char *lastbuf = last6->sin6_addr.s6_addr;
	unsigned char *maskbuf = mask6->sin6_addr.s6_addr;
	short i;

	for(i = 0; i < 16; i++) firstbuf[i] &= maskbuf[i];

	if(!itf->ifa_broadaddr){
	    memcpy(&net->last.ip.v6, &net->first.ip.v6,
		   sizeof(net->last.ip.v6));
	    for(i = 0; i < 16; i++) lastbuf[i] |= ~maskbuf[i];
	}
    }

    _inet_addr_inc(&net->first);
    _inet_addr_dec(&net->last);
    return net;
}

int inet_iterator_init(inet_iterator_t* it, inet_network_t* net,
                       inet_addr_t* start, inet_addr_t* end){
    if(!it || !net) return -1;
    if(!start) start = &net->here;
    if(!end) end = start;

    it->start = start;
    it->end = end;

    inet_iterator_reset(it);
    return 0;
}

int inet_iterator_next(inet_iterator_t* it, inet_network_t* net){
    if(!it) return 0;

    unsigned char fresh = it->current.ip.raw.sa_family == 0;
    if(!fresh && _inet_addr_cmp(it->end, &it->current) == 0)
	return 0;

    if(fresh) memcpy(&it->current, it->start, sizeof(it->current));

    if(_inet_addr_cmp(&it->current, &net->last) == 0)
	memcpy(&it->current, &net->first, sizeof(it->current));
    else _inet_addr_inc(&it->current);

    return fresh || _inet_addr_cmp(it->end, &it->current) != 0;
}

int inet_iterator_reset(inet_iterator_t* it){
    if(!it) return -1;
    memset(&it->current, 0, sizeof(it->current));
    return 0;
}
