/* This file is part of libinetiter.
 * Copyright (C) 2017 Julien JPK <julienjpk@email.com>

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or any later version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.

 * You should have received a copy of the GNU Affero General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef _INETITER_H
#define _INETITER_H

#include <stdlib.h>
#include <string.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <arpa/inet.h>

/** \brief An IP address (binary representation).
 *
 * The purpose of this structure is to act as a wrapper around the "standard" 
 * sockaddr_* structures, which suffer from heavy casting/overflowing when 
 * manipulated outside of typical system calls such as bind.
 *
 * Access to the IP must be made once the family has been determined. It can be 
 * obtained from the union using myip.ip.v4 or myip.ip.v6. The raw member can be
 * used to determine the family: myip.ip.raw.sa_family. 
 *
 * I'd recommend using the functions defined in this file whenever possible, and
 * avoid accessing this structure's members manually. */
typedef struct inet_addr{
    /** The address itself, accessed differently based on its family. */
    union {
	/** A "typeless" representation of the address. */
	struct sockaddr raw;
	/** The IPv4 version of the IP, if applicable. */
	struct sockaddr_in v4;
	/** The IPv6 version of the IP, if applicable. */
	struct sockaddr_in6 v6;
    } ip;
} inet_addr_t;

/** \brief An IP network, spanning from the network IP (+1) to the  broadcast 
 *        address (-1). 
 *
 * The idea of a broadcast is a bit different with IPv6, so the end 
 * address is computed differently. "here" represents your current host (with 
 * regards to the network).
 *
 * Note: this library doesn't support iterating over network you're not 
 * currently part of (have an interface for). This is because the inet_network 
 * structure is initialised from the ifaddrs struct (see getifaddrs(3)). Future 
 * version might allow manual passing of the network parameters. */
typedef struct inet_network{
    /** The smallest available IP on the network. */
    inet_addr_t first;
    /** The current host's IP on the network. */
    inet_addr_t here;
    /** The largest available IP on the network. */
    inet_addr_t last;
} inet_network_t;

/** \brief An iterator over the IPs of a inet_network_t.
 *
 * This structure can be used to iterate over all possible IPs on a given 
 * network. It should not be manipulated directly, except when accessing the 
 * "current" member (read-only). */
typedef struct inet_iterator{
    /** The iterator's starting point. */
    inet_addr_t* start;
    /** The iterator's stopping point. */
    inet_addr_t* end;
    /** The current IP (this member is meant to be accessed). */
    inet_addr_t current;
} inet_iterator_t;

/** \brief Retrieves the (standard) size of an IP representation, in bytes. */
socklen_t inet_addr_sizeof(inet_addr_t* addr);

/** \brief Sets the port on a ::inet_addr_t struct. */
void inet_addr_set_port(inet_addr_t* addr, in_port_t port);

/** \brief Returns a dynamically-allocated buffer containing the IP's text 
 *         representation.
 *
 * This buffer is allocated through malloc(3) based on the address family, and 
 * should be free-d when no longer needed. */
char* inet_addr_str(inet_addr_t* addr, char* buffer);

/** \brief Builds a inet_network_t struct from a given interface. */
inet_network_t* inet_network_new(struct ifaddrs* itf);

/** \brief Initialises an iterator struct for a given network.
 *
 * The start and end parameters may be omitted. The iterator will then start at 
 * the current address (here), and loop back to it. */
int inet_iterator_init(inet_iterator_t* it, inet_network_t* net,
                       inet_addr_t* start, inet_addr_t* end);

/** \brief Advances the iterator by one IP.
 *
 * The current IP can be retrieved as it->current. The it->current.ip.raw struct
 * can be used with Linux system calls (struct sockaddr) such as bind(2). Note 
 * that for now, iterators do NOT include the current host (here) and start at 
 * the IP right after it. */
int inet_iterator_next(inet_iterator_t* it, inet_network_t* net);

/** \brief Resets an operator.
 *
 * Basically the same as ::inet_iterator_init, but reuses the previously passed 
 * network, start and end settings. */
int inet_iterator_reset(inet_iterator_t* it);

#endif
