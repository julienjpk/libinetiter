# Network exploration in C with libinetiter #

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](http://www.gnu.org/licenses/agpl-3.0)

`libinetiter` is a small library allowing you to iterate over all available IPs
on a given network. It uses the associated network interface to determine all
the necessary information, and returns an iterator structure.

This library was written for one of my own programs, I can therefore not ensure
that it will work exactly as you expect it to. Don't hesitate to tweak it a bit,
it's just two files after all.

## Installing ##

Requires CMake >= 2.6.

    $ mkdir build
    $ cd build
    $ cmake ../
    $ make

## Documentation ##

If you need more details on how to use it, change into the `doc/` directory and
run Doxygen:

    $ doxygen

It will generate the library's documentation in HTML and LaTeX formats.
